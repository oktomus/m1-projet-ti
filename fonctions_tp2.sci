
function img = filtre_kb(dg_img, p)
//
// In:  Image grayscale et en double
//      p : Taille du filtre [1, ...]
// Out: Image filtré entre [p, width -p] x [p, height - p]
//

    [iwidth, iheight] = size(dg_img);
    
    img = dg_img;
    
    x = 1 + p;
    while x <= iwidth - p do
        y = 1 + p;
        while y <= iheight - p do
            fenetre = double(dg_img(x - p:x + p, y - p:y + p));
            pmax = max(fenetre);
            pmin = min(fenetre);
            if dg_img(x, y) >= (pmax + pmin) / 2 then
                img(x, y) = pmax;
            else
                img(x, y) = pmin;
            end
            y = y + 1;
        end
        x = x + 1;
    end
endfunction


function img = filtre_median(dg_img, p)
//
// In:  Image grayscale et en double
//      p : Taille du filtre [1, ...]
// Out: Image filtré entre [p, width -p] x [p, height - p]
//

    [iwidth, iheight] = size(dg_img);
    
    img = dg_img;
    
    x = 1 + p;
    while x <= iwidth - p do
        y = 1 + p;
        while y <= iheight - p do
            fenetre = dg_img(x - p:x + p, y - p:y + p);
            img(x, y) = median(fenetre);
            y = y + 1;
        end
        x = x + 1;
    end
endfunction


function img = filtre_lisseur(dg_img, p)
//
// In:  Image grayscale et en double
//      p : Taille du filtre [1, ...]
// Out: Image lissé
//
    filtre = 1/(p * p) * ones(p, p);
    img = imfilter(dg_img, filtre);
endfunction
