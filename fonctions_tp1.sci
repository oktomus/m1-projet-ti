function img = dgimread(path)
//
// Charche l'image avec le chemain donné et recupère la luminance dans [0, 1]
//    
    img = imread(path);
    img = rgb2gray(img);
    img = double(img) / 255.0;
endfunction

function freqq = fouriervisu_nolog(dg_img)
//
// In: Grayscale image en double
// Out: Grayscale image de la transformée de fourier de l'image
//
    freqq =  1 + abs(fftshift(fft2(dg_img)));
    fmax = max(freqq);
    fmin = min(freqq);  
    ratio = (1.0 / (fmax - fmin));
    freqq = ratio * (freqq - fmin);
endfunction

function img = fouriervisu(freqq)
//
// In: FFT
// Out: Grayscale image de la transformée de fourier de l'image
//
    img = log(1 + abs(freqq));
    fmax = max(img);
    fmin = min(img);  
    ratio = (1.0 / (fmax - fmin));
    img = ratio * (img - fmin);
endfunction

function img = img_fouriervisu(dg_img)
//
// In: Grayscale image en double
// Out: Grayscale image de la transformée de fourier de l'image
//
    img = fouriervisu(fftshift(fft2(dg_img)));  
endfunction
