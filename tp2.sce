// Dépendances
exec("fonctions_tp2.sci");
stacksize(100000000);

// Kramer et Bruckner

bois = dgimread("img/bois.jpeg");
bois_gaussian = imnoise(bois, "gaussian", 0, 0.01);
bois_salt = imnoise(bois, "salt & pepper", 0.001);

imwrite(bois_gaussian, "export/tp2/bois_gaussian.png");
imwrite(bois_gaussian, "export/tp2/bois_salt_pepper.png");

// 7)

for p = 1:5
    path_bois = strcat(["export/tp2/kb_bois_p", string(p), ".png"]);
    path_bois_gaussian = strcat(["export/tp2/kb_bois_gaussian_p", string(p), ".png"]);
    path_bois_saltpepper = strcat(["export/tp2/kb_bois_saltpepper_p", string(p), ".png"]);
    imwrite(filtre_kb(bois, p), path_bois);
    imwrite(filtre_kb(bois_gaussian, p), path_bois_gaussian);
    imwrite(filtre_kb(bois_salt, p), path_bois_saltpepper);
end

// 8)

for p = 1:5
    for l = 1:5
        path_bois = strcat(["export/tp2/kb_mean_bois_l", string(l), "_p", string(p), ".png"]);
        path_bois_gaussian = strcat(["export/tp2/kb_mean_bois_gaussian_l", string(l), "_p", string(p), ".png"]);
        path_bois_saltpepper = strcat(["export/tp2/kb_mean_bois_saltpepper_l", string(l), "_p", string(p), ".png"]);
        imwrite(filtre_kb(filtre_lisseur(bois, l), p), path_bois);
        imwrite(filtre_kb(filtre_lisseur(bois_gaussian, l), p), path_bois_gaussian);
        imwrite(filtre_kb(filtre_lisseur(bois_salt, l), p), path_bois_saltpepper);
    end
end
