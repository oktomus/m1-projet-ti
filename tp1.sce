// Dépendances
exec("fonctions_tp1.sci");
stacksize(100000000);

// Images
lena = dgimread("img/lena.png");
bois = dgimread("img/bois.jpeg");
crepis = dgimread("img/crepis-chaux.jpg");
laine = dgimread("img/laine.jpg");
cryo = dgimread("img/ima1_v2.png");

// 4) Visualisation des transformations de fourrier
freqq = img_fouriervisu(lena);
imwrite(freqq, "export/tp1/fourier_lena.png");

freqq = img_fouriervisu(bois);
imwrite(freqq, "export/tp1/fourier_bois.png");

freqq = img_fouriervisu(crepis);
imwrite(freqq, "export/tp1/fourier_crepis.png");

freqq = img_fouriervisu(laine);
imwrite(freqq, "export/tp1/fourier_laine.png");

freqq = img_fouriervisu(cryo);
imwrite(freqq, "export/tp1/fourier_cryo.png");

// 5) Transformée inverse
freqq = ifft(fft2(lena) * 400);
imwrite(fouriervisu(freqq), "export/tp1/5_lena_inverse.png");
