Kevin MASSON
2017 - 2018
Traitement d'image, rendu

# Transformée de Fourier, Implémentation

** 1. Pour une image en niveau de gris, traduire en fran ̧cais la commande suivante (log(1+abs(fftshift(fft2(Im))))) , puis afficher le résultat. **

Transformée de Fourier rapide sur l'image *Im*, centrage de la transformée de fourrier. Puis récupération de la phases de nombres complexes pour pouvoir les afficher.

** 2. Expliquer le rôle de chaque fonction. **

- *fft2*: Transformée de Fourier rapide en 2D
- *fftshifht*: Décallage des valeurs pour les centrer
- *abs*: Récupération du module
- *log* et *1 +*: Attenue les hautes fréquences

Il faut normaliser les valeurs pour les afficher correctement.

** 3. en fairne une fonction fouriervisu **

Voir les fichiers
- `tp1.sce`
- `fonctions_tp1.sci`


** 4. Pour chacune de ces images réelles crepis, laine, CryoEM, afficher leur TFD et interpréter. **

Lancer `tp1.sce` et voir les fichiers dans le dossier export.

Interpretation :

- Crepis: De trés grandes variations horizontales et verticales. On peut aussi voir une lueur blanche légèrement en diagonale, cela correspond à la direction de la lumière principale.
- Cryo: Beaucoup de bruit donc on a des fréquences basses, moyennes et hautes. On peut aussi voir des abérations chromatiques que l'on ne remarque pas dans l'image originale. 
- Bois: Beaucoup de fréquences moyenes circulaires, on reconnait bien les formes circulaires du bois
- Lena: Beaucoup de variations horizontales, verticales et diagonales car l'image contient beaucoup de bord.
- Laine: Comme l'image cryo, on a toute sorte de fréquence

** 5. **

Multiplication de la fréquence par 400 pour avoir une image plus claire.

# Filtrage non-linéaire

## Filtre de Kramer et Bruckner

Marche très mal sur des images bruitées.

Marche bien avec un pré-filtre lisseur. Avec un grand filtre de lissage, on obtient un effet peinture.

##  Filtre de Pérona-Malik
